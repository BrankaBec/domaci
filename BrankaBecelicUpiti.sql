SELECT proizvod.naziv, drzava.kod
FROM proizvod 
JOIN proizvodjac ON proizvod.proizvodjacID = proizvodjac.proizvodjacID
JOIN grad ON proizvodjac.gradID = grad.ID
JOIN drzava ON drzava.kod = grad.drzavaID
ORDER BY drzava.kod;

SELECT count(proizvodID), proizvodjac.naziv
FROM proizvod 
JOIN proizvodjac ON proizvod.proizvodjacID = proizvodjac.proizvodjacID
GROUP BY proizvodjac.naziv;

SELECT count(proizvodID), proizvodjac.naziv
FROM proizvod 
JOIN proizvodjac ON proizvod.proizvodjacID = proizvodjac.proizvodjacID
GROUP BY proizvodjac.naziv;

SELECT proizvod.naziv, prodavnica.naziv
FROM prodavnica_proizvod
JOIN prodavnica ON prodavnica.prodavnicaID = prodavnica_proizvod.proizvodId
JOIN proizvod ON prodavnica_proizvod.proizvodId = proizvod.proizvodID
WHERE prodavnica.naziv = 'Dis2';

SELECT proizvod.naziv, kategorija.naziv
FROM proizvod
JOIN kategorija ON kategorija.kategorijaID = proizvod.kategorijaID
WHERE kategorija.naziv = 'Konditorski proizvodi';

SELECT count(proizvodId), kategorija.naziv
From proizvod
JOIN kategorija ON proizvod.kategorijaID = kategorija.kategorijaID
GROUP BY kategorija.naziv;




